package main

import (
	"context"
	"log"
	"sync"
	"time"

	ros "bitbucket.org/mfkenney/go-ros"
	tcm "bitbucket.org/mfkenney/go-tcm"
	api "bitbucket.org/uwaploe/go-covis/pkg/translator"
	"google.golang.org/grpc/grpclog"
)

type rotServer struct {
	devs     [3]ros.Rotator
	sensor   *tcm.Tcm
	offline  map[int]bool
	mu       sync.Mutex
	debug    bool
	tcmSync  bool
	seekMode bool
}

type dataSample struct {
	axis  int
	angle float32
}

func NewServer(devs map[string]ros.Rotator, sens *tcm.Tcm, offline []int) *rotServer {
	s := rotServer{}
	for axis, r := range devs {
		if id := rotatorId(axis); id >= 0 {
			s.devs[id] = r
		} else {
			log.Printf("Invalid axis: %q", axis)
		}
	}

	s.offline = make(map[int]bool)
	for _, n := range offline {
		if n >= 0 {
			s.offline[n] = true
		}
	}

	s.sensor = sens
	return &s
}

func (s *rotServer) inMotion() bool {
	var state bool
	for id, dev := range s.devs {
		if !s.offline[id] {
			state = state || dev.IsMoving()
		}
	}
	return state
}

func (s *rotServer) setDebug(state bool) {
	s.debug = state
}

func (s *rotServer) getRotators() (api.Rotation, error) {
	r := api.Rotation{}
	if !s.offline[api.ROLL] {
		dev := s.devs[api.ROLL]
		x, err := dev.Position()
		if err != nil {
			return r, err
		}
		r.Roll = int32(float64(x) * api.ANGLESCALE)
		shift := uint(api.ROLL)
		if dev.IsMoving() {
			r.Inmotion |= (1 << shift)
		}
		if dev.HasStalled() {
			r.Stalled |= (1 << shift)
			log.Println("Slip/stall detected on Roll rotator")
		}
	}

	if !s.offline[api.TILT] {
		dev := s.devs[api.TILT]
		x, err := dev.Position()
		if err != nil {
			return r, err
		}
		r.Tilt = int32(float64(x) * api.ANGLESCALE)
		shift := uint(api.TILT)
		if dev.IsMoving() {
			r.Inmotion |= (1 << shift)
		}
		if dev.HasStalled() {
			r.Stalled |= (1 << shift)
			log.Println("Slip/stall detected on Tilt rotator")
		}
	}

	if !s.offline[api.PAN] {
		dev := s.devs[api.PAN]
		x, err := dev.Position()
		if err != nil {
			return r, err
		}
		r.Pan = int32(float64(x) * api.ANGLESCALE)
		shift := uint(api.PAN)
		if dev.IsMoving() {
			r.Inmotion |= (1 << shift)
		}
		if dev.HasStalled() {
			r.Stalled |= (1 << shift)
			log.Println("Slip/stall detected on Pan rotator")
		}
	}

	return r, nil
}

func (s *rotServer) getOrientation() (api.Orientation, error) {
	o := api.Orientation{}
	if s.sensor == nil {
		return o, nil
	}

	var (
		values []tcm.DataValue
		err    error
	)

	if s.tcmSync {
		values, err = s.sensor.SyncRead()
	} else {
		values, err = s.sensor.GetData()
	}

	if err != nil {
		return o, err
	}

	for _, dv := range values {
		switch dv.Id {
		case tcm.Heading:
			if x, ok := dv.Data.(float32); ok {
				o.Heading = int32(float64(x) * api.ANGLESCALE)
			}
		case tcm.Pangle:
			if x, ok := dv.Data.(float32); ok {
				o.Pitch = int32(float64(x) * api.ANGLESCALE)
			}
		case tcm.Rangle:
			if x, ok := dv.Data.(float32); ok {
				o.Roll = int32(float64(x) * api.ANGLESCALE)
			}
		}
	}

	return o, nil
}

func (s *rotServer) GetRotation(ctx context.Context, _ *api.Empty) (*api.Rotation, error) {
	if s.debug {
		log.Println("GetRotation()")
	}
	s.mu.Lock()
	r, err := s.getRotators()
	s.mu.Unlock()
	return &r, err
}

func (s *rotServer) GetAttitude(ctx context.Context, _ *api.Empty) (*api.Orientation, error) {
	if s.debug {
		log.Println("GetAttitude()")
	}
	s.mu.Lock()
	o, err := s.getOrientation()
	s.mu.Unlock()
	return &o, err
}

func (s *rotServer) GetState(ctx context.Context, _ *api.Empty) (*api.State, error) {
	if s.debug {
		log.Println("GetState()")
	}
	state := api.State{}
	r, err := s.getRotators()
	if err != nil {
		return &state, err
	}
	state.R = &r
	o, err := s.getOrientation()
	if err != nil {
		return &state, err
	}
	state.O = &o
	return &state, nil
}

type sendFn func(*api.State) error

func (s *rotServer) streamData(ctx context.Context, interval time.Duration, fsend sendFn) error {
	ticker := time.NewTicker(interval)
	defer ticker.Stop()
	for s.inMotion() {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
			o, _ := s.getOrientation()
			r, _ := s.getRotators()
			if err := fsend(&api.State{R: &r, O: &o}); err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *rotServer) Move(o *api.Orientation, stream api.Translator_MoveServer) error {

	if s.debug {
		log.Printf("Move(%#v)", *o)
	}
	s.mu.Lock()
	defer s.mu.Unlock()

	ctx := context.Background()

	// The desired movement is expressed in attitude sensor space, so we
	// need to convert to rotator space before adjusting.
	if !s.offline[api.ROLL] && (o.Roll > 200 || o.Roll < -200) {
		dev := s.devs[api.ROLL]
		x := float32(api.ROLL2ROLL * float64(o.Roll) / api.ANGLESCALE)
		err := dev.Move(ctx, x, !s.seekMode)
		if err != nil {
			grpclog.Printf("Move(%#v) Roll adjust failed: %v", *o, err)
			return err
		}
	}
	if !s.offline[api.TILT] && (o.Pitch > 200 || o.Pitch < -200) {
		dev := s.devs[api.TILT]
		x := float32(api.PITCH2TILT * float64(o.Pitch) / api.ANGLESCALE)
		err := dev.Move(ctx, x, !s.seekMode)
		if err != nil {
			grpclog.Printf("Move(%#v) Tilt adjust failed: %v", *o, err)
			return err
		}
	}
	if !s.offline[api.PAN] && (o.Heading > 200 || o.Heading < -200) {
		dev := s.devs[api.PAN]
		x := float32(api.HDG2PAN * float64(o.Heading) / api.ANGLESCALE)
		err := dev.Move(ctx, x, !s.seekMode)
		if err != nil {
			grpclog.Printf("Move(%#v) Pan adjust failed: %v", *o, err)
			return err
		}
	}

	return s.streamData(ctx, time.Second, stream.Send)
}

func (s *rotServer) Goto(r *api.Rotation, stream api.Translator_GotoServer) error {
	if s.debug {
		log.Printf("Goto(%#v)", *r)
	}
	s.mu.Lock()
	defer s.mu.Unlock()

	ctx := context.Background()

	if !s.offline[api.ROLL] && r.Roll > 0 {
		dev := s.devs[api.ROLL]
		x := float32(float64(r.Roll) / api.ANGLESCALE)
		// Convert absolute angle to relative
		angle, _ := dev.Position()
		err := dev.Move(ctx, x-angle, !s.seekMode)
		if err != nil {
			grpclog.Printf("Goto(%#v) Roll adjust failed: %v", *r, err)
			return err
		}
	}

	if !s.offline[api.TILT] && r.Tilt > 0 {
		dev := s.devs[api.TILT]
		x := float32(float64(r.Tilt) / api.ANGLESCALE)
		// Convert absolute angle to relative
		angle, _ := dev.Position()
		err := dev.Move(ctx, x-angle, !s.seekMode)
		if err != nil {
			grpclog.Printf("Goto(%#v) Tilt adjust failed: %v", *r, err)
			return err
		}
	}

	if !s.offline[api.PAN] && r.Pan > 0 {
		dev := s.devs[api.PAN]
		x := float32(float64(r.Pan) / api.ANGLESCALE)
		// Convert absolute angle to relative
		angle, _ := dev.Position()
		err := dev.Move(ctx, x-angle, !s.seekMode)
		if err != nil {
			grpclog.Printf("Goto(%#v) Pan adjust failed: %v", *r, err)
			return err
		}
	}

	return s.streamData(ctx, time.Second, stream.Send)
}

func (s *rotServer) Level(_ *api.Empty, stream api.Translator_LevelServer) error {

	if s.debug {
		log.Println("Level()")
	}
	s.mu.Lock()
	defer s.mu.Unlock()

	ctx := context.Background()

	o, err := s.getOrientation()
	if err != nil {
		grpclog.Printf("Level() cannot read TCM: %v", err)
		return err
	}
	o.Heading = 0
	o.Pitch = -o.Pitch
	o.Roll = -o.Roll

	if !s.offline[api.ROLL] && (o.Roll > 200 || o.Roll < -200) {
		dev := s.devs[api.ROLL]
		x := float32(api.ROLL2ROLL * float64(o.Roll) / api.ANGLESCALE)
		err := dev.Move(ctx, x, false)
		if err != nil {
			grpclog.Printf("Level() Roll adjust failed: %v", err)
			return err
		}
	}
	if !s.offline[api.TILT] && (o.Pitch > 200 || o.Pitch < -200) {
		dev := s.devs[api.TILT]
		x := float32(api.PITCH2TILT * float64(o.Pitch) / api.ANGLESCALE)
		err := dev.Move(ctx, x, false)
		if err != nil {
			grpclog.Printf("Level() Tilt adjust failed: %v", err)
			return err
		}
	}

	return s.streamData(ctx, time.Second, stream.Send)
}

func (s *rotServer) SetControl(ctx context.Context, ctl *api.Control) (*api.Control, error) {
	if s.debug {
		log.Printf("SetControl(%#v)", *ctl)
	}
	s.mu.Lock()
	defer s.mu.Unlock()

	m := ros.Motion{
		Vmax:  float32(float64(ctl.Speed) / api.CTLSCALE),
		Accel: float32(float64(ctl.Accel) / api.CTLSCALE),
		Brake: int(ctl.Brake),
	}

	m_cur, err := s.devs[0].Motion()
	if err != nil {
		grpclog.Printf("SetControl(%#v) error: %v", *ctl, err)
		return nil, err
	}

	rval := api.Control{
		Speed: int32(float64(m_cur.Vmax) * api.CTLSCALE),
		Accel: int32(float64(m_cur.Accel) * api.CTLSCALE),
		Brake: int32(m_cur.Brake),
	}

	for i, dev := range s.devs {
		if !s.offline[i] {
			err := dev.SetMotion(m)
			if err != nil {
				grpclog.Printf("SetControl(%#v) error: %v", *ctl, err)
				return &rval, err
			}
		}
	}

	return &rval, nil
}

func (s *rotServer) SetMode(ctx context.Context, mode *api.Mode) (*api.Mode, error) {
	if s.debug {
		log.Printf("SetMode(%#v)", *mode)
	}

	rval := api.Mode{UseSeek: s.seekMode}
	s.seekMode = mode.UseSeek
	return &rval, nil
}

func (s *rotServer) AllStop(ctx context.Context, _ *api.Empty) (*api.State, error) {
	if s.debug {
		log.Println("AllStop()")
	}
	s.mu.Lock()
	defer s.mu.Unlock()

	for i, dev := range s.devs {
		if !s.offline[i] {
			m, err := dev.Motion()
			if err != nil {
				grpclog.Printf("AllStop() error: %v", err)
				return nil, err
			}
			dev.Stop(m.Brake, false)
		}
	}
	return s.GetState(ctx, &api.Empty{})
}
