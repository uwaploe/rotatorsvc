package main

import (
	"io/ioutil"
	"strings"

	ros "bitbucket.org/mfkenney/go-ros"
	api "bitbucket.org/uwaploe/go-covis/pkg/translator"
	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
)

type rotatorConfig struct {
	Device  string     `toml:"device"`
	Baud    int        `toml:"baud"`
	Accel   float32    `toml:"accel"`
	Maxvel  float32    `toml:"maxvel"`
	Brake   int        `toml:"brake"`
	Limits  ros.Limits `toml:"limits"`
	Timeout int        `toml:"timeout"`
}

type sensorConfig struct {
	Device      string `toml:"device"`
	Baud        int    `toml:"baud"`
	Orientation string `toml:"orientation"`
	Sync        bool   `toml:"sync"`
}

type sysConfig struct {
	Address  string                   `toml:"address"`
	Rotators map[string]rotatorConfig `toml:"rotators"`
	Sensor   sensorConfig             `toml:"sensor"`
}

func rotatorId(axis string) int {
	switch strings.ToLower(axis) {
	case "roll":
		return api.ROLL
	case "tilt", "pitch":
		return api.TILT
	case "pan", "heading":
		return api.PAN
	}

	return -1
}

func loadConfig(filename string) (sysConfig, error) {
	var cfg sysConfig
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return cfg, errors.Wrap(err, "read file")
	}
	err = toml.Unmarshal(b, &cfg)
	return cfg, err
}
