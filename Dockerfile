FROM scratch
COPY rotatorsvc /
EXPOSE 10130
EXPOSE 6060
ENTRYPOINT ["/rotatorsvc"]
