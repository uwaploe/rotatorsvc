// Grpc service for COVIS rotator control
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	ros "bitbucket.org/mfkenney/go-ros"
	tcm "bitbucket.org/mfkenney/go-tcm"
	api "bitbucket.org/uwaploe/go-covis/pkg/translator"
	"github.com/pkg/errors"
	"github.com/tarm/serial"
	"google.golang.org/grpc"
)

var usage = `Usage: rotatorsvc [options] cfgfile

Grpc service for COVIS rotator control.
`

// Default data record components
const TCM_FIELDS = "heading,pangle,rangle"

var Version = "dev"
var BuildDate = "unknown"

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	debugMode = flag.Bool("debug", false, "Enable debugging output")
)

type axisList []int

func (a *axisList) String() string {
	return fmt.Sprint(*a)
}

func (a *axisList) Set(value string) error {
	if len(*a) > 0 {
		return errors.New("flag already set")
	}
	for _, name := range strings.Split(value, ",") {
		*a = append(*a, rotatorId(name))
	}
	return nil
}

var offline axisList

func init() {
	flag.Var(&offline, "offline", "Comma-separated list of offline rotators")
}

func initRotators(cfgs map[string]rotatorConfig) (map[string]ros.Rotator, error) {
	devs := make(map[string]ros.Rotator)

	for axis, cfg := range cfgs {
		p, err := serial.OpenPort(&serial.Config{
			Name:        cfg.Device,
			Baud:        cfg.Baud,
			ReadTimeout: time.Second * time.Duration(cfg.Timeout)})
		if err != nil {
			return devs, err
		}
		m := ros.Motion{
			Vmax:  cfg.Maxvel,
			Accel: cfg.Accel,
			Brake: cfg.Brake,
		}
		devs[axis] = ros.NewRotator(p, "A")

		err = devs[axis].SetMotion(m)
		if err != nil {
			return devs, errors.Wrap(err, "set motion")
		}
		devs[axis].SetLimits(cfg.Limits)
		if err != nil {
			return devs, errors.Wrap(err, "set limits")
		}

		log.Printf("%s: %#v", axis, devs[axis].Settings())
	}

	return devs, nil
}

func initTcm(cfg sensorConfig) (*tcm.Tcm, error) {
	fields := strings.Split(TCM_FIELDS, ",")
	p, err := serial.OpenPort(&serial.Config{
		Name:        cfg.Device,
		Baud:        cfg.Baud,
		ReadTimeout: time.Second * 5})
	if err != nil {
		return nil, err
	}
	dev := tcm.NewTcm(p)
	err = dev.SetDataComponents(tcm.ComponentIds(fields...)...)
	if err != nil {
		return dev, errors.Wrap(err, "SetDataComponents")
	}
	mref, err := tcm.MountingRefValue(cfg.Orientation)
	if err != nil {
		return dev, errors.Wrap(err, "mounting ref")
	}
	err = dev.SetConfig(tcm.Kmountingref, mref)
	if err != nil {
		return dev, errors.Wrap(err, "SetConfig")
	}
	err = dev.SetAcqParams(tcm.Polled, true, 0, 0)
	if err != nil {
		return dev, errors.Wrap(err, "SetAcqParams")
	}
	err = dev.Save()
	if err != nil {
		return dev, errors.Wrap(err, "Save")
	}

	return dev, nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}
	args := flag.Args()

	if len(args) == 0 {
		fmt.Fprintln(os.Stderr, "Missing configuration file")
		flag.Usage()
		os.Exit(1)
	}

	cfg, err := loadConfig(args[0])
	if err != nil {
		log.Fatal(err)
	}

	rs, err := initRotators(cfg.Rotators)
	if err != nil {
		log.Fatalf("initRotators failed: %v", err)
	}

	if len(rs) == 0 {
		log.Fatalln("No rotators defined")
	}

	sens, err := initTcm(cfg.Sensor)
	if err != nil {
		log.Fatalf("initTcm failed: %v", err)
	}

	if cfg.Sensor.Sync {
		err = sens.SyncMode()
		if err != nil {
			log.Fatalf("Cannot put TCM in sync mode: %v", err)
		}
		defer sens.NormalMode()
	}

	lis, err := net.Listen("tcp", cfg.Address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	svc := NewServer(rs, sens, offline)
	svc.tcmSync = cfg.Sensor.Sync
	svc.setDebug(*debugMode)

	opts := make([]grpc.ServerOption, 0)
	grpcServer := grpc.NewServer(opts...)
	api.RegisterTranslatorServer(grpcServer, svc)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s := <-sigs
		log.Printf("Got signal: %v", s)
		grpcServer.GracefulStop()
	}()

	log.Printf("Rotator gRPC Server %s starting", Version)
	grpcServer.Serve(lis)
}
