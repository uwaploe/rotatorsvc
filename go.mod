module bitbucket.org/uwaploe/rotatorsvc

require (
	bitbucket.org/mfkenney/go-ros v0.6.0
	bitbucket.org/mfkenney/go-tcm v1.0.0
	bitbucket.org/uwaploe/go-covis v1.15.1
	github.com/BurntSushi/toml v0.3.1
	github.com/pkg/errors v0.8.1
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/net v0.0.0-20190311183353-d8887717615a
	google.golang.org/grpc v1.21.1
)

go 1.13
